---
title: "Github R CRAN"
description: "Search GitHub for R Code from CRAN packages"
slug: "github-r-cran"
keywords:
  - "GitHub"
  - "R"
  - "CRAN"
categories:
  - "OpenSearch plugins"
date: 2018-01-25T01:30:14+01:00
draft: false
---

Lets you search through all {{% a_blank "R" "https://www.r-project.org" %}} Code (`language:R`) from {{% a_blank "CRAN" "https://cran.r-project.org" %}} packages (`user:cran`) on {{% a_blank "GitHub" "https://github.com" %}}.

Inspired by this tweet from Noam Ross: {{< tweet 563422536633839617 >}}

