---
title: "Wikipedia (en)"
description: "Search for English Wikipedia articles"
slug: "wikipedia-en"
keywords:
  - "Wikipedia"
  - "English"
categories:
  - "OpenSearch plugins"
date: 2018-01-25T00:12:08+01:00
draft: false
---

Enables you to search for articles in the {{% a_blank "English Wikipedia" "https://en.wikipedia.org" %}}.

