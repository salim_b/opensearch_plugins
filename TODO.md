# TODOs

- [ ] Firefox removed the `window.external.AddSearchProvider()` function in version 78. Adapt the site to use the [new auto-discovery feature (`<link rel="search">` tag)](https://support.mozilla.org/en-US/questions/1292034#answer-1326154) instead.
