// code borrowed from https://stackoverflow.com/a/13348618/7196903
function isEdge() {
  var winNav = window.navigator,
    isIEedge = winNav.userAgent.indexOf("Edge") > -1;

  if (isIEedge) {
    return true;
  } else {
    return false;
  }
}

function isChrome() {
  var isChromium = window.chrome,
    winNav = window.navigator,
    vendorName = winNav.vendor,
    isOpera = winNav.userAgent.indexOf("OPR") > -1,
    isIEedge = winNav.userAgent.indexOf("Edge") > -1,
    isIOSChrome = winNav.userAgent.match("CriOS");

  if (isIOSChrome) {
    return true;
  } else if (
    isChromium !== null &&
    typeof isChromium !== "undefined" &&
    vendorName === "Google Inc." &&
    isOpera === false &&
    isIEedge === false
  ) {
    return true;
  } else {
    return false;
  }
}

function installSearchEngine(search_plugin_url) {
  if (window.external && ("AddSearchProvider" in window.external)) {
    // Microsoft Edge
    if (isEdge()) {
      alert("Microsoft Edge doesn't support adding search plugins manually.\n\nFor more information see:\nhttps://msdn.microsoft.com/en-us/library/aa744112(v=vs.85).aspx\n\nConsider using Mozilla Firefox instead:\nhttps://firefox.com");
    // TODO: handle Safari separately (doesn't support OpenSearch natively but we could refer to [this plugin](http://www.opensearchforsafari.com/) if adding plugins from subpages is supported)
    } else if (isChrome()) {
      alert("Chrome/Chromium doesn't support adding search plugins manually anymore.\n\nFor more information see:\nhttps://stackoverflow.com/q/41281475/7196903\n\nConsider using Mozilla Firefox instead:\nhttps://firefox.com");
    } else {
      // supported browsers like Firefox 2+ and IE 7+
      window.external.AddSearchProvider(search_plugin_url);
    }
  } else {
    // No search engine support (IE 6, Opera, Safari etc).
    alert("Your browser doesn't support search plugins.\n\nConsider updating it or just use Mozilla Firefox:\nhttps://firefox.com");
  }
}
